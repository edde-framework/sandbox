<?php
	declare(strict_types = 1);

	use Edde\Api\Application\IApplication;
	use Edde\Api\Cache\ICacheFactory;
	use Edde\Api\Cache\ICacheStorage;
	use Edde\Api\File\IRootDirectory;
	use Edde\Common\Cache\CacheFactory;
	use Edde\Common\File\RootDirectory;
	use Edde\Common\Runtime\Runtime;
	use Edde\Ext\Cache\InMemoryCacheStorage;
	use Sandbox\SandboxSetup;
	use Tracy\Debugger;

	require_once __DIR__ . '/vendor/autoload.php';
	require_once __DIR__ . '/src/loader.php';

	Debugger::enable(Debugger::DEVELOPMENT, __DIR__ . '/logs');
	Debugger::$strictMode = true;

	$factoryList = array_merge([
		IRootDirectory::class => new RootDirectory(__DIR__),
	], is_array($local = @include __DIR__ . '/loader.local.php') ? $local : []);

	try {
		Runtime::execute(SandboxSetup::create($factoryList[ICacheFactory::class] ?? new CacheFactory(__DIR__, $factoryList[ICacheStorage::class] ?? new InMemoryCacheStorage()), $factoryList), function (IApplication $application) {
			$application->run();
		});
	} catch (\Exception $exception) {
		Debugger::log($exception);
		throw $exception;
	}
