<?php
	declare(strict_types = 1);

	use Edde\Common\Autoloader;

	Autoloader::register(__NAMESPACE__, __DIR__, false);
