<?php
	declare(strict_types = 1);

	namespace Sandbox;

	use Edde\Api\Cache\ICacheFactory;
	use Edde\Api\Container\IContainer;
	use Edde\Api\File\IRootDirectory;
	use Edde\Api\Identity\IAuthenticatorManager;
	use Edde\Api\Link\ILinkFactory;
	use Edde\Api\Router\IRouterService;
	use Edde\Api\Runtime\ISetupHandler;
	use Edde\Api\Schema\ISchemaFactory;
	use Edde\Api\Template\ITemplateManager;
	use Edde\Api\Upgrade\IUpgradeManager;
	use Edde\Common\Container\Factory\ClassFactory;
	use Edde\Common\Link\ControlLinkGenerator;
	use Edde\Common\Link\HttpLinkGenerator;
	use Edde\Ext\Router\EddeRouter;
	use Edde\Ext\Router\RestRouter;
	use Edde\Ext\Runtime\DefaultSetupHandler;
	use Edde\Ext\Upgrade\InitialStorageUpgrade;
	use Edde\Ext\Upgrade\UpgradeHandler;
	use Sandbox\Login\SimpleAuthenticator;
	use Sandbox\Rest\SandboxService;
	use Sandbox\Upgrade\InitialUpgrade;

	class SandboxSetup extends DefaultSetupHandler {
		static public function create(ICacheFactory $cacheFactory = null, array $factoryList = []): ISetupHandler {
			return parent::create($cacheFactory)
				->registerFactoryList(array_merge([
					RestRouter::class,
					new ClassFactory(),
				], $factoryList))
				->deffered(ISchemaFactory::class, function (ICacheFactory $cacheFactory, IRootDirectory $rootDirectory, ISchemaFactory $schemaFactory) {
					$cache = $cacheFactory->factory(__DIR__);
					/** @var $schemaList array */
					if (($schemaList = $cache->load('schema-list')) === null) {
						$schemaList = [];
						foreach ($rootDirectory as $file) {
							if (strpos($path = $file->getPath(), '-schema.php') === false) {
								continue;
							}
							$schemaList[] = $path;
						}
						$cache->save('schema-list', $schemaList);
					}
					foreach ($schemaList as $schema) {
						$schemaFactory->load($schema);
					}
				})
				->deffered(IRouterService::class, function (IContainer $container, IRouterService $routerService) {
					$routerService->registerRouter($container->create(RestRouter::class));
					$routerService->registerRouter($container->create(EddeRouter::class));
				})
				->deffered(IUpgradeManager::class, function (IContainer $container, IUpgradeManager $upgradeManager) {
					$upgradeManager->registerUpgrade($container->create(InitialStorageUpgrade::class, '0.0'));
					$upgradeManager->registerUpgrade($container->create(InitialUpgrade::class, '1.0'));
					$upgradeManager->listen($container->create(UpgradeHandler::class));
				})
				->deffered(ILinkFactory::class, function (IContainer $container, ILinkFactory $linkFactory) {
					$linkFactory->registerLinkGenerator($container->create(ControlLinkGenerator::class));
					$linkFactory->registerLinkGenerator($container->create(HttpLinkGenerator::class));
				})
				->deffered(IAuthenticatorManager::class, function (IContainer $container, IAuthenticatorManager $authenticatorManager) {
					$authenticatorManager->registerAuthenticator($container->create(SimpleAuthenticator::class));
					$authenticatorManager->registerFlow(SimpleAuthenticator::class);
				})
				->deffered(ITemplateManager::class, function (ITemplateManager $templateManager) {
				})
				->deffered(RestRouter::class, function (IContainer $container, RestRouter $restRouter) {
					$restRouter->registerService($container->create(SandboxService::class));
				});
		}
	}
