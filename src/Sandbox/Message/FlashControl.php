<?php
	declare(strict_types = 1);

	namespace Sandbox\Message;

	use Edde\Common\Html\Tag\DivControl;

	/**
	 * Simple flash message control implementation.
	 */
	class FlashControl extends DivControl {
	}
