<?php
	declare(strict_types = 1);

	namespace Sandbox\Login;

	return [
		'name' => 'LoginCrate',
		'namespace' => __NAMESPACE__,
		'property-list' => [
			[
				'name' => 'login',
			],
			[
				'name' => 'password',
			],
		],
	];
