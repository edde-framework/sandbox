<?php
	declare(strict_types = 1);

	namespace Sandbox\Rest;

	use Edde\Api\Url\IUrl;
	use Edde\Common\Rest\AbstractService;

	class SandboxService extends AbstractService {
		public function match(IUrl $url): bool {
			return strpos($url->getPath(), '/rest/v1/sandbox') !== false;
		}

		public function restGet() {
			$this->response(static::class . ' rest api!', 'text/plain');
		}
	}
